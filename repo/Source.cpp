#include "Source.h"
#include <sstream>
#include <tchar.h>


#define PATH_MAX 2048

typedef int(*pdisconnect)(void);


void main()
{
	menu();
}

void menu()
{
	string inp;

	while (true)
	{
		cout << "Enter your command: ";
		getline(cin, inp);
		handle_command(Command(inp));
	}
}

void handle_command(Command& cmd)
{
	string command = cmd.getCommand();

	if (command == "pwd")
	{
		commitPwd();
	}

	else if (command == "cd")
	{
		string dir = (cmd.getArgs().size() > 0) ? cmd.getArgs()[0] : "";
		commitCd(dir);
	}

	else if (command == "create")
	{
		string dir = (cmd.getArgs().size() > 0) ? cmd.getArgs()[0] : "";
		commitCreate(dir);
	}

	else if (command == "ls")
	{
		commitLs();
	}

	else if (command == "answer")
	{
		commitAnswer();
	}

	else if (command == "execute")
	{
		string dir = (cmd.getArgs().size() > 0) ? cmd.getArgs()[0] : "";
		commitExecute(dir);
	}

	else
	{
		cout << "Unknown command." << endl;
	}
}


void commitPwd()
{
	CHAR buffer[PATH_MAX] = { 0 };
	DWORD rc = GetCurrentDirectoryA(PATH_MAX, buffer);
	if (rc == 0)
	{
		cout << "Error printing the current path.\n error code: " << GetLastError() << endl;
		return;
	}

	cout << string(buffer) << endl;
	
}

void commitCd(string dir)
{
	if (dir.length() == 0)
	{
		cout << "Error: Empty directory." << endl;
		return;
	}

	string newdir = dir.substr(0, MAX_PATH);
	BOOL rc = SetCurrentDirectoryA(newdir.c_str());
	if (!rc)
	{
		cout << "Error changing directory. error code: " << GetLastError() << endl;
		return;
	}

	cout << "Directory successfully changed to " << newdir << endl;
}

void commitCreate(string fileName)
{
	if (fileName.length() == 0)
	{
		cout << "Error: Empty file name." << endl;
		return;
	}

	HANDLE newFile = CreateFileA(fileName.c_str(), GENERIC_WRITE, 0, NULL,  CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, NULL);

	if (newFile == INVALID_HANDLE_VALUE)
	{
		cout << "Error opening the file. Error code:" << GetLastError() << endl;
		return;
	}

	cout << "File " << fileName << " has been successfully created." << endl;
	CloseHandle(newFile);
}

void commitLs()
{
	WIN32_FIND_DATA ffd;
	HANDLE hFind = INVALID_HANDLE_VALUE;

	CHAR buffer[PATH_MAX] = { 0 };
	DWORD rc = GetCurrentDirectoryA(PATH_MAX, buffer);

	string newDir(buffer);
	newDir += "\\*";

	if (rc == 0)
	{
		cout << "Error getting the current path.\n error code: " << GetLastError() << endl;
		return;
	}


	hFind = FindFirstFile(newDir.c_str(), &ffd);

	do
	{
		if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		{
			cout <<"Folder: " <<  ffd.cFileName << endl;
		}

		else
		{
			cout << "File: " << ffd.cFileName << endl;
		}

		
	} while (FindNextFile(hFind, &ffd));

	if (GetLastError() != ERROR_NO_MORE_FILES)
	{
		cout << "ls error code: " << GetLastError() << endl;
	}

	FindClose(hFind);

	cout << endl << endl;

}

void commitAnswer()
{
	HMODULE dll = LoadLibrary(_T("Secret.dll"));
	pdisconnect func;

	if (dll)
	{
		func = (pdisconnect)GetProcAddress(dll, "TheAnswerToLifeTheUniverseAndEverything");
		if (func)
		{
			cout << "The answer is " << func() << "!" << endl;
		}
	}

	else
	{
		cout << "Error loading the dll file." << endl;
	}
	
}

void commitExecute(string fileName)
{

	if (fileName.length() == 0)
	{
		cout << "Error: Empty file name." << endl;
		return;
	}

	HINSTANCE fileHandle = ShellExecute(NULL, "open", fileName.c_str(), NULL, NULL, SW_SHOWNORMAL);

	if ((int) fileHandle <= 32)
	{
		cout << "Error executing a file. Error code: " << GetLastError() << endl;
		return;
	}

	DWORD rc = WaitForSingleObject(fileHandle, INFINITE);

	if (rc == WAIT_FAILED)
	{
		cout << "Wait failed. Error code: " << GetLastError() << endl;
		
		return;
	}

	DWORD exitCode;
	GetExitCodeProcess(fileHandle, &exitCode);
	cout << "Process exited with : " << exitCode << endl;

}