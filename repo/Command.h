#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Command
{
private:
	string _cmd;
	vector<string> _params;
public:
	Command(string command);
	~Command();
	string getCommand();
	vector<string> getArgs();

};