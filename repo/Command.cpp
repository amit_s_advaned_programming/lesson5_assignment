#include "Command.h"
#include "Helper.h"

Command::Command(string command) 
{
	vector<string> totalVec = Helper::get_words(command);
	//Creation of the arguments vector:
	vector<string>::const_iterator first = totalVec.begin() + 1;
	vector<string>::const_iterator last = totalVec.end();
	vector<string> argsVec(first, last);

	_cmd = totalVec[0];
	_params = argsVec;
}

Command::~Command() {}

string Command::getCommand()
{
	return _cmd;
}

vector<string> Command::getArgs()
{
	return _params;
}