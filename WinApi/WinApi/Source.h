#pragma once
#include <iostream>
#include <string>
#include "Helper.h"
#include "Command.h"
#include "Windows.h"

void menu();
void handle_command(Command& cmd);
void commitPwd();
void commitCd(string dir);
void commitCreate(string fileName);
void commitLs();
void commitAnswer();
void commitExecute(string fileName);